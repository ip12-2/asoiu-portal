<?php 

    $item = ITWork::GetCompany(array('id' => $id));
    $founder_user = APIUser::Get(array('id'=>$item->founder_id));

	if(!empty($_POST['send'])){
		$item_send = ITWork::EditCompany(array(
			'id' => $id,
			'company_name' => $_POST['company_name'],
			'address' => $_POST['address'],
			'photo_url' => $_POST['photo_url'],
			'company_url' => $_POST['company_url'],
			'foundation_date' => $_POST['foundation_date'],
			'founder_id' => user('id'),
			'is_contract_verified' => 0,
			'contract_url' => 0
		));	
		echo "<script>document.location.href=\"http://asoiu.com/itworks/".$item_send->id."\"</script>";
	}

?>

<div class="container" style="margin-top: 65px;">
    <div class="row">
		<h1><?php echo lang('Редагувати компанію:', 'Редактировать компанию:'); ?></h1>
		<form method="POST" style="width:40%">
			
  <div class="form-group">
			<input type="text" name="company_name" value="<?php echo $item->company_name; ?>" class="form-control"/>
			</div>
  <div class="form-group">
			<textarea name="address"  class="form-control" rows="3"><?php echo $item->address; ?></textarea>
			</div>
  <div class="form-group">
			<input type="text" name="photo_url" value="<?php echo $item->photo_url; ?>" class="form-control"/>
			</div>
  <div class="form-group">
			<input type="text" name="company_url" value="<?php echo $item->company_url; ?>" class="form-control"/>
			</div>
  <div class="form-group">
			<input name="foundation_date" value="<?php echo date("d F Y",$item->foundation_date); ?>" class="form-control datepicker"/>
			</div>
			
  <div class="form-group">
			<input type="hidden" name="send" value="true"/>
		
		<input type="submit" class="btn btn-default" value="<?php echo lang('Зберегти', 'Сохранить'); ?>"/>
			</div>
		
		</form>
		<br><br><br><br>
	</div>
</div>
