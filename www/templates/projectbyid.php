<?php 
    $item = ITWork::GetProject(array('id' => $id));
    $company = ITWork::GetCompany(array('id'=>$item->company_id));
    $founder_user = APIUser::Get(array('id'=>$company->founder_id));
?>

<div class="container" style="margin-top: 150px;">
    <?php if ($founder_user->id == user('id')) { ?>
    <a href="<?php echo $id; ?>/worker" class="btn btn-primary pull-right" style="margin: 0 0 10px 10px;"><?php echo lang('Додати співробітника', 'Добавить сотрудника') ?></a>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="container-fluid well"  <?php if ($item->is_contract_verified) echo 'style="background: #FFF8D4;"'; ?>>
                <div class="row-fluid">
                    <div class="col col-lg-2" >
                        <img src="<?php echo $item->photo_url; ?>" class="img-circle" width="140" height="140">
                    </div>
                    
                    <div class="col col-lg-6">
                        <h3><?php echo $item->project_name; ?></h3>
                        <h6>Компания: <a href="/itworks/<?php echo $item->company_id; ?>"><?php echo $company->company_name; ?></a></h6>
                        <h6>Дата основания: <?php echo date("d F Y",$item->start_date); ?></h6>
                        <h6>Проект завершен: <?php echo date("d F Y",$item->end_date); ?></h6>
                        <h6>Адресс: <a href="http://<?php echo $item->project_url; ?>"><?php echo $item->project_url; ?></a></h6>
                    </div>

                    
                    
                    <!-- <div class="span2">
                        <div class="btn-group">
                            <a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
                                Action 
                                <span class="icon-cog icon-white"></span><span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="icon-wrench"></span> Modify</a></li>
                                <li><a href="#"><span class="icon-trash"></span> Delete</a></li>
                            </ul>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>          
    </div>
        <?php $issue = ITWork::GetProjectStudents(array("id"=>$id));
            if($issue->error->code == 200):?>
                <h1>Никто тут не работает</h1>
            <?php else:?>
        <h1>Работают:</h1>
            <?php foreach($issue as $item): ?>
		<div class="col-lg-4">
                    <a href="/user/<?php echo $item->id; ?>">
                        <img class="img-circle" src="<?php echo $item->photo_url; ?>" style="width: 140px; height: 140px;">
                        <h3><?php echo $item->first_name." ".$item->last_name; ?></h3>
                    </a>
                </div>
            <?php endforeach; ?>
        <?php endif;?>
        </div>

</div>
