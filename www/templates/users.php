
<div class="container marketing" style="margin-top: 90px;">
    <form method="post" class="form-inline" style="margin: 15px;">
        <select name="role" class="form-control">
            <option <?php if($_POST['role']==10 || empty($_POST['role'])) echo 'selected'; ?> value="10"><?php echo lang('Усі', 'Все'); ?></option>
            <option <?php if($_POST['role']==9) echo 'selected'; ?> value="9"><?php echo lang('Студенти', 'Студенты'); ?></option>
            <option <?php if($_POST['role']==8) echo 'selected'; ?> value="8"><?php echo lang('Викладачі', 'Преподаватели'); ?></option>
            <option <?php if($_POST['role']==5) echo 'selected'; ?> value="5"><?php echo lang('Адміністрація', 'Администрация'); ?></option>
        </select>
        <input type="submit" class="btn btn-default" value="<?php echo lang('Фільтрувати', 'Фильтровать'); ?>">
    </form>
    <?php $users = APIUser::Get(array('role' => $_POST['role']));
    if($users->error->code == 4):
    ?>
    <div class="page-header">
        <h1>Пользователи не найдены</h1>
        <p class="lead">Возможно Вы выберите другие параметры поиска?</p>
    </div>
    <?php else: ?>
    <div class="container">
        <style>
            .glyphicon {  margin-bottom: 10px;margin-right: 10px;}

            small {
                display: block;
                line-height: 1.428571429;
                color: #999;
            }
        </style>
        <div class="row">
<?php foreach($users as $user): ?>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <a href="/user/<?php echo $user->id; ?>"><img src="<?php echo $user->photo_url; ?>" alt="" class="img-rounded img-responsive" /></a>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <h4><a href="/user/<?php echo $user->id; ?>"><?php echo $user->first_name." ".$user->last_name;  if($user->id == user()->id) echo " (".lang("Це Ви", "Это Вы").")";?></a></h4>
                            <small><cite title="KPI, ФИОТ">ФИОТ, КПИ <i class="glyphicon glyphicon-map-marker">
                                    </i></cite></small>
                            <p>
                                <i class="glyphicon glyphicon-envelope"></i><?php echo $user->login; ?>
                                <br />
                                <i class="glyphicon glyphicon-globe"></i><a href="http://www.vk.com/anarh93">www.vk.com</a>
                                <br />
                                <i class="glyphicon glyphicon-gift"></i><?php echo date('d F Y', $user->birthday); ?></p>
                            <!-- Split button -->
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary">
                                    Social</button>
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span><span class="sr-only">Social</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Twitter</a></li>
                                    <li><a href="#">Google +</a></li>
                                    <li><a href="#">Facebook</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Github</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--h4>Работает в:</h4-->
            <?php $issue = array(); //Issue::Get(array("uid"=>$user->id));?>
<?php foreach($issue as $item): ?>
            <!--div class="col-lg-2">
                <img class="img-circle" src="<?php echo $item->photo; ?>" style="width: 140px; height: 140px;">
                <h4><?php echo mb_substr($item->title, 0, 12, "utf-8"); ?></h4>
                <p><a class="btn btn-success" href="/issue/<?php echo $item->id; ?>" role="button">Присоединиться »</a></p>
            </div-->
        <?php endforeach; ?>
<?php endforeach; ?>


        </div>
    </div>
<?php endif; ?>

</div>