<?php 
    $item = ITWork::GetCompany(array('id' => $id));
    $founder_user = APIUser::Get(array('id'=>$item->founder_id));
?>

<div class="container" style="margin-top: 150px;">
    <?php if ($founder_user->id == user('id')) { ?>
    <a href="<?php echo $id; ?>/edit" class="btn btn-success pull-right" style="margin: 0 0 10px 10px;"><?php echo lang('Редагувати інформацію про компанію', 'Редактировать информацию о компании') ?></a>
    <a href="<?php echo $id; ?>/project" class="btn btn-info pull-right" style="margin: 0 0 10px 10px;"><?php echo lang('Додати проект', 'Добавить проект') ?></a>    
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="container-fluid well"  <?php if ($item->is_contract_verified) echo 'style="background: #FFF8D4;"'; ?>>
                <div class="row-fluid">
                    <div class="col col-lg-2" >
                        <img src="<?php echo $item->photo_url; ?>" class="img-circle" width="140" height="140">
                    </div>
                    
                    <div class="col col-lg-6">
                        <h3><?php echo $item->company_name; ?></h3>
                        <h6>Основатель: <a href="/user/<?php echo $founder_user->id; ?>"><?php echo $founder_user->first_name." ".$founder_user->last_name; ?></a></h6>
                        <h6>Дата основания: <?php echo date("d F Y",$item->foundation_date); ?></h6>
                        <h6>Адресс: <?php echo $item->address; ?></h6>
                        <h6>Контракт: <a href="http://<?php echo $item->contract_url; ?>"><?php echo $item->contract_url; ?></a></h6>
                        <h6><a href="http://<?php echo $item->company_url; ?>"><?php echo $item->company_url; ?></a></h6>
                    </div>

                    
                    
                    <!-- <div class="span2">
                        <div class="btn-group">
                            <a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
                                Action 
                                <span class="icon-cog icon-white"></span><span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><span class="icon-wrench"></span> Modify</a></li>
                                <li><a href="#"><span class="icon-trash"></span> Delete</a></li>
                            </ul>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>  
    </div>
    <div style="clear: both;"></div>
        <?php $issue = ITWork::GetCompanyProject(array("id"=>$id));
            if($issue->error->code == 200):?>
                <h1>Проектов нет</h1>
            <?php else:?>
        <h1>Проекты:</h1>
            <?php foreach($issue as $item): ?>
		<div class="col-lg-4">
                    <a href="/itworks/project/<?php echo $item->id; ?>">
                        <img class="img-circle" src="<?php echo $item->photo_url; ?>" style="width: 140px; height: 140px;">
                        <h3><?php echo $item->first_name." ".$item->last_name; ?></h3>
                    </a>
                </div>
            <?php endforeach; ?>
        <?php endif;?>    <div style="clear: both;"></div>
        <?php $issue = ITWork::GetCompanyStudents(array("id"=>$id));
            if($issue->error->code == 200):?>
                <h1>Никто тут не работает</h1>
            <?php else:?>
        <h1>Работают:</h1>
            <?php foreach($issue as $item): ?>
		<div class="col-lg-4">
                    <a href="/user/<?php echo $item->id; ?>">
                        <img class="img-circle" src="<?php echo $item->photo_url; ?>" style="width: 140px; height: 140px;">
                        <h3><?php echo $item->first_name." ".$item->last_name; ?></h3>
                    </a>
                </div>
            <?php endforeach; ?>
        <?php endif;?>

</div>
