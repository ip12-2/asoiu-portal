<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>АСОИУ</title>

        <!-- Bootstrap -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/carousel.css" rel="stylesheet">
        <link href="/css/datepicker.css" rel="stylesheet">
		

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?112"></script>
        <script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>
		

        <script type="text/javascript">
            VK.init({apiId: 4337828, onlyWidgets: true});

            $(function() {
				//$('.datepicker').datepicker();
                $('a[href=#vk_login]').click(function() {
                    VK.Auth.login(function(response) {
                        if (response.session) {
                            console.log(response.session.user);
                            var url = 'http://asoiu.com/login';
                            var form = $('<form action="' + url + '" method="post" style="display:none;">' +
                                    '<input type="text" name="type" value="1" />' +
                                    '<input type="text" name="sid" value="' + response.session.user.id + '" />' +
                                    '<input type="text" name="login" value="' + response.session.user.domain + '" />' +
                                    '<input type="text" name="first_name" value="' + response.session.user.first_name + '" />' +
                                    '<input type="text" name="last_name" value="' + response.session.user.last_name + '" />' +
                                    '<input type="text" name="photo_url" value="http://forum.vgd.ru/no_avatar.png" />' +
                                    '</form>');
                            $('body').append(form);
                            $(form).submit();
                            if (response.settings) {
                                console.log(response.settings);
                            }
                        }
                    });
                });
            });

        </script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="navbar-wrapper">
            <div class="container">

                <div class="navbar navbar-inverse navbar-static-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/">#АСОИУ</a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <!--li><a href="/add"><?php echo lang('Повідомити', 'Сообщить'); ?></a></li>
                                <li><a href="/"><?php echo lang('Проблеми', 'Проблемы'); ?></a></li-->
                                <li><a href="/user/"><?php echo lang('Користувачі', 'Пользователи'); ?></a></li>
                                <li><a href="/about"><?php echo lang('Про проект', 'Про проект'); ?></a></li>
                                <li><a href="/documents"><?php echo lang('Документи', 'Документы'); ?></a></li>
                                <li><a href="/itworks"><?php echo lang('Робота', 'Работа'); ?></a></li>
                                <li class="dropdown">
                                    <a href="#login" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('Авторизація', 'Авторизация'); ?><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <?php if (isOnline()): ?>
                                            <li><a href="/logout">Выход</a></li>
                                        <?php else: ?>
                                            <li><a href="#vk_login">ВКонтакте</a></li>
                                            <li><a href="#fb">Facebook</a></li>
                                            <li><a href="#tw">Twitter</a></li>
                                        <?php endif; ?>
                                        <li class="divider"></li>
                                        <li class="dropdown-header"><?php echo lang('Мова', 'Язик'); ?></li>
                                        <li><a href="?lang=ua"><?php echo lang('Україньська', 'Украинская'); ?></a></li>
                                        <li><a href="?lang=ru"><?php echo lang('Російська', 'Русская'); ?></a></li>
                                    </ul>
                                </li>
                            </ul>
                            <?php if (isOnline()): ?>
                                <?php $user = user(null	); ?>
                                <a class="navbar-brand" href="/user/<?php echo $user->id; ?>" style="float: right;margin-right: 15px;"><?php echo $user->first_name . " " . $user->last_name; ?>
                                    <img src="<?php echo $user->photo_url; ?>" style="width: 30px; height: 30px;margin-top: -3px;"></a>
                                <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
