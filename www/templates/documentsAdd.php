<?php
        $users = APIUser::Get(array('role'=>8));
	if(!empty($_POST['send'])){
		$uploaddir = 'uploads/';
		$uploadfile = $uploaddir . basename($_FILES['logo']['name']);
		move_uploaded_file($_FILES['logo']['tmp_name'], $uploadfile);
		$item = Documents::Create(array(
			'name' => $_POST['name'],
			'pic' => "http://asoiu.com/".$uploadfile,
			'resp' => $_POST['resp']
		));	
		echo "<script>document.location.href=\"http://asoiu.com/documents/\"</script>";
	}
?>

<div class="container" style="margin-top: 65px;">
    <div class="row">
		<h1><?php echo lang('Створити новий документ:', 'Создать новый документ'); ?></h1>
		<form method="POST" style="width:40%" enctype="multipart/form-data">
			
  <div class="form-group">
			<input type="text" name="name" placeholder="<?php echo lang('Назва документу', 'Имя документа'); ?>" class="form-control"/>
			</div>
  <div class="form-group">
			<!-- <input type="text" name="pic" placeholder="<?php echo lang('Посилання', 'Ссылка'); ?>" class="form-control"/> -->
			<input type="hidden" name="MAX_FILE_SIZE" value="300000" />
			<?php echo lang('Посилання', 'Ссылка'); ?>: <input name="logo" type="file" />
			</div>
  <div class="form-group">
                        <?php echo lang('Відповідальний:', 'Ответственный:'); ?>
			<!--input type="text" name="student_id" placeholder="<?php echo lang('id працівника', 'id сотрудника'); ?>" class="form-control"/-->
                        <select name="resp" class="form-control">
                            <?php foreach($users as $user):?>
                            <option value="<?php echo $user->id; ?>"><?php echo $user->first_name. " ". $user->last_name; ?></option>
                            <?php endforeach;?>
                        </select>
			</div>
			
  <div class="form-group">
			<input type="hidden" name="send" value="true"/>
		
		<input type="submit" class="btn btn-default" value="Создать"/>
			</div>
		
		</form>
		<br><br><br><br>
	</div>
</div>
