<div class="container" style="margin-top: 150px;">

    <?php 
        if (isOnline()) {
            if (user()->role<9) { 
    ?>
    <a href="/documents/add" class="btn btn-success pull-right" style="margin: 0 0 10px 10px;"><?php echo lang('Додати документ', 'Добавить документ') ?></a>    
    <?php 
            } 
    ?>
    <div class="row">
        <table class="table">
            <tr>
                <th><?php echo lang('Назва документу','Имя документа'); ?></th>
                <th><?php echo lang('Посилання','Ссылка'); ?></th>
                <th><?php echo lang('Відповідальний','Ответственный'); ?></th>
                <th><?php echo lang('Стадія','Этап'); ?></th>
                <th></th>
            </tr>
            <?php 
                $items = Documents::GetAll(null);
                foreach($items as $item) :
                ?>
            <tr>
                <!--td><?php echo $item->id;?></td-->
                <td><?php echo $item->name;?></td>
                <td><a href="<?php echo $item->pic;?>"><?php echo $item->pic;?></a></td>
                <td><?php echo '<a href="/user/'.$item->uid."\">".$item->first_name." ".$item->last_name."</a>";?></td>
                <td><?php echo $item->done == 0?"Необробленний":"Обробленний";?></td>
                <td style="text-align: center;">
                    <a href="/documents/<?php echo $item->id; ?>/edit" class="btn btn-info btn-xs" style="margin: 5px;"><?php echo lang('Редагувати', 'Редактировать') ?></a>
                    <a href="/documents/<?php echo $item->id; ?>/delete" class="btn btn-danger btn-xs" style="margin: 5px;"><?php echo lang('Видалити', 'Удалить') ?></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <?php
        }
        else {
    ?>
    <h1><?php echo lang('Немає доступних для перегляду документів.', 'Нет доступных для просмотра документов.'); ?></h1>
    <?php
        }
    ?>
    
</div>
