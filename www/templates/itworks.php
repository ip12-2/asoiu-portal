<?php 
	$itworks = ITWork::GetAllCompany(null);
?>

<div class="container" style="margin-top: 150px;">
	<div class="row">
		<?php foreach($itworks as $item) :
			$founder_user = APIUser::Get(array('id'=>$item->founder_id));
		?>
		<div class="col-md-6">
			<div class="container-fluid well"  <?php if ($item->is_contract_verified) echo 'style="background: #FFF8D4;"'; ?>>
				<div class="row-fluid">
			        <div class="col col-lg-4" >
					    <img src="<?php echo $item->photo_url; ?>" class="img-circle" width="140" height="140">
			        </div>
			        
			        <div class="col col-lg-6">
			            <h3><a href="/itworks/<?php echo $item->id; ?>"><?php echo $item->company_name; ?></a></h3>
			            <h6>Основатель: <a href="/user/<?php echo $founder_user->id; ?>"><?php echo $founder_user->first_name." ".$founder_user->last_name; ?></a></h6>
			            <h6>Дата основания: <?php echo date("d F Y",$item->foundation_date); ?></h6>
			            <h6>Адресс: <?php echo $item->address; ?></h6>
        				<h6>Контракт: <a href="http://<?php echo $item->contract_url; ?>"><?php echo $item->contract_url; ?></a></h6>
                        <h6><a href="http://<?php echo $item->company_url; ?>"><?php echo $item->company_url; ?></a></h6>
			        </div>

			        
			        
			        <!-- <div class="span2">
			            <div class="btn-group">
			                <a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#">
			                    Action 
			                    <span class="icon-cog icon-white"></span><span class="caret"></span>
			                </a>
			                <ul class="dropdown-menu">
			                    <li><a href="#"><span class="icon-wrench"></span> Modify</a></li>
			                    <li><a href="#"><span class="icon-trash"></span> Delete</a></li>
			                </ul>
			            </div>
			        </div> -->
				</div>
			</div>
		</div>			
		<?php endforeach; ?>
	</div>
		

</div>
