<?php
	$document = Documents::Get(array('id' => $id ));

        $users = APIUser::Get(array('role'=>8));
	if(!empty($_POST['send'])){
		$uploaddir = 'uploads/';
		$uploadfile = $uploaddir . basename($_FILES['pic']['name']);
		move_uploaded_file($_FILES['pic']['tmp_name'], $uploadfile);
		$item = Documents::Edit(array(
			'id' => $id,
			'name' => $_POST['name'],
			'pic' => "http://asoiu.com/".$uploadfile,
			'resp' => $_POST['resp'],
			'done' => $_POST['done']
		));	
		echo "<script>document.location.href=\"http://asoiu.com/documents/\"</script>";
	}
?>

<div class="container" style="margin-top: 65px;">
    <div class="row">
		<h1><?php echo lang('Редагувати документ:', 'Редактировать документ'); ?></h1>
		<form method="POST" style="width:40%" enctype="multipart/form-data">
			
  <div class="form-group">
			<input type="text" name="name" value="<?php echo $document->name; ?>" class="form-control"/>
			</div>
  <div class="form-group">
      <a href="<?php echo $document->pic; ?>"><?php echo $document->pic; ?></a><br>
			<input type="hidden" name="MAX_FILE_SIZE" value="300000" />
			<?php echo lang('Посилання', 'Ссылка'); ?>: <input name="pic" type="file"/>
			</div>
  <div class="form-group">
			<!--input type="text" name="student_id" placeholder="<?php echo lang('id працівника', 'id сотрудника'); ?>" class="form-control"/-->
                        <select name="resp" class="form-control">
                            <?php foreach($users as $user):?>
                            <option value="<?php echo $user->id; ?>" <?php if($document->resp==$user->id):?> selected="selected"<?php endif;?>><?php echo $user->first_name. " ". $user->last_name; ?></option>
                            <?php endforeach;?>
                        </select>
			</div>
  <div class="form-group">
                        <select name="done" class="form-control">
                            <option value="1" <?php if($document->done==1):?> selected="selected"<?php endif;?>><?php echo lang('Обробленний', 'Обработан'); ?></option>
                            <option value="0" <?php if($document->done==0):?> selected="selected"<?php endif;?>><?php echo lang('Необробленний', 'Необработан'); ?></option>
                        </select>
			</div>
			
  <div class="form-group">
			<input type="hidden" name="send" value="true"/>
		
		<input type="submit" class="btn btn-success" value="<?php echo lang('Зберегти', 'Сохранить'); ?>"/>
			</div>
		
		</form>
		<br><br><br><br>
	</div>
</div>
