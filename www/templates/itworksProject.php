<?php 
    $founder_user = APIUser::Get(array('id'=>$item->founder_id));
    
                $lat_filename = prepareFileName($_FILES['logo']['name']);
                $uploaddir = 'uploads/';
                //$uploadfile = $uploaddir . basename($_FILES['logo']['name']);
                move_uploaded_file($_FILES['logo']['tmp_name'], $uploaddir . $lat_filename);
                
	if(!empty($_POST['send'])){
		$item_send = ITWork::AddProject(array(
			'company_id' => $id,
			'project_name' => $_POST['project_name'],
			'photo_url' => "http://asoiu.com/uploads/".$lat_filename,
			'project_url' => $_POST['project_url'],
			'start_date' => $_POST['start_date'],
			'end_date' => $_POST['end_date']
		));	
		echo "<script>document.location.href=\"http://asoiu.com/itworks/project/".$item_send->id."\"</script>";
	}

?>

<div class="container" style="margin-top: 65px;">
    <div class="row">
		<h1><?php echo lang('Додати проект:', 'Добавить проект:'); ?></h1>
		<form enctype="multipart/form-data" method="POST" style="width:40%">
			
  <div class="form-group">
			<input type="text" name="project_name" placeholder="<?php echo lang('Ім\'я проекту', 'Название проекта'); ?>" class="form-control"/>
			</div>
  <div class="form-group">
			<input type="text" name="project_url" placeholder="<?php echo lang('Сайт проекту', 'Сайт проекта'); ?>" class="form-control"/>
			</div>
  <div class="form-group">
    <input type="hidden" name="MAX_FILE_SIZE" value="300000" />
    Логотип компании: <input name="logo" type="file" />
			<!--input type="text" name="photo_url" placeholder="Логотип компании" class="form-control"/-->
			</div>
  <div class="form-group">
			<input name="start_date" placeholder="<?php echo lang('Дата початку', 'Дата начала'); ?>" class="form-control datepicker"/>
			</div>
  <div class="form-group">
			<input name="end_date" placeholder="<?php echo lang('Дата кінця', 'Дата конца'); ?>" class="form-control datepicker"/>
			</div>
			
  <div class="form-group">
			<input type="hidden" name="send" value="true"/>
		
		<input type="submit" class="btn btn-default" value="<?php echo lang('Додати', 'Добавить'); ?>"/>
			</div>
		
		</form>
		<br><br><br><br>
	</div>
</div>
