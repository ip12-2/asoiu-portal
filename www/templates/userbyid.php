
    <div class="container marketing" style="margin-top: 50px;">
	
		<?php
		
			$user = is_int($id) ? user() :  APIUser::Get(array('id'=>$id));
		
			if($user->error->code == 200):?>
				<div class="page-header">
        <h1>Такого юзера нет</h1>
        <p class="lead">Возможно Вы вернётесь на главную и поисчите что-то ещё?</p>
      </div>
			<?php else:?>
	<div class="container">
<style>body{padding-top:30px;}

.glyphicon {  margin-bottom: 10px;margin-right: 10px;}

small {
display: block;
line-height: 1.428571429;
color: #999;
}</style>

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="<?php echo $user->photo_url; ?>" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4><?php echo $user->first_name." ".$user->last_name; if($user->id == user()->id) echo " (".lang("Це Ви", "Это Вы").")";?></h4>
                        <small><cite title="KPI, ФИОТ">ФИОТ, КПИ <i class="glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                            <i class="glyphicon glyphicon-envelope"></i><?php echo $user->login; ?>
                            <br />
                            <i class="glyphicon glyphicon-globe"></i><a href="http://www.vk.com/anarh93">www.vk.com</a>
                            <br />
                            <i class="glyphicon glyphicon-gift"></i><?php echo date('d F Y',$user->birthday); ?></p>
                        <!-- Split button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">
                                Social</button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span><span class="sr-only">Social</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Google +</a></li>
                                <li><a href="#">Facebook</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Github</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .glyphicon{margin: 0px;}
        </style>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <table class="table" style="width: 95%; margin: 0 15px;">
            <tr>
                <th><?php echo lang('Назва документу','Имя документа'); ?></th>
                <th><?php echo lang('Відповідальний','Ответственный'); ?></th>
                <th><?php echo lang('Стадія','Этап'); ?></th>
                <th></th>
            </tr>
            <?php 
                $items = Documents::GetAll(null);
                foreach($items as $item) :
                    if($item->uid != $id)
                                                continue;
                ?>
            <tr>
                <td><a href="<?php echo $item->pic;?>"><?php echo $item->name;?></a></td>
                <td><?php echo '<a href="/user/'.$item->uid."\">".$item->first_name." ".$item->last_name."</a>";?></td>
                <td><?php echo $item->done == 0?"<span class=\"glyphicon glyphicon-remove\" style=\"color:red;\"></span>":"<span class=\"glyphicon glyphicon-ok\" style=\"color:green;\"></span>";?></td>
                <td style="text-align: center;">
                    <a href="/documents/<?php echo $item->id; ?>/edit" class="btn btn-info btn-xs" style="margin: 5px;"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="/documents/<?php echo $item->id; ?>/delete" class="btn btn-danger btn-xs" style="margin: 5px;"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
                </div>
            </div>
        </div>  
        <div style="clear:both;"/></div>
    
    <div class="col-lg-6">
	<?php $issue = ITWork::GetStudentCompanies(array("id"=>$user->id));
            if($issue->error->code == 200):?>
                <h1>Не работает</h1><a href="/itworks/add" class="btn btn-default">Додати нову компанію</a>
            <?php else:?>
        <h1>Работает в:&nbsp;&nbsp;&nbsp;<a href="/itworks/add" class="btn btn-default">Додати нову компанію</a></h1>
            <?php foreach($issue as $item): ?>
		<div class="col-lg-4">
                    <img class="img-circle" src="<?php echo $item->photo_url; ?>" style="width: 140px; height: 140px;">
                    <h3><?php echo mb_substr($item->company_name, 0, 15, "utf-8"); ?></h3>
                    <p><a class="btn btn-success" href="/itworks/<?php echo $item->id; ?>" role="button">Посмотреть »</a></p>
                </div>
            <?php endforeach; ?>
        <?php endif;?>
        </div>
    
    <div class="col-lg-6">
	<?php $issue = ITWork::GetStudentProjects(array("id"=>$user->id));
            if($issue->error->code == 200):?>
                <h1>Не участвует ни в каких проектах</h1>
            <?php else:?>
        <h1>Занят в проектах:</h1>
            <?php foreach($issue as $item): ?>
		<div class="col-lg-4">
                    <img class="img-circle" src="<?php echo $item->photo_url; ?>" style="width: 140px; height: 140px;">
                    <h3><?php echo mb_substr($item->project_name, 0, 12, "utf-8"); ?></h3>
                    <p><a class="btn btn-success" href="/itworks/project/<?php echo $item->id; ?>" role="button">Посмотреть »</a></p>
                </div>
            <?php endforeach; ?>
        <?php endif;?>
        </div>
    </div>


      
	<?php endif; ?>
	
	</div>