<!DOCTYPE HTML>
<html>
    <head>
        <title>Test page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script src="js/html5.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/jquery.form.min.js"></script>

        <script>
            $(function(){
                $('form').submit(function(){
                    var request = $.ajax({
                        type: "POST",
                        url: "http://api.asoiu.com/methods/auth."+$('input[name="method"]').val(),
                        dataType: "json",
                        crossDomain: true,
                        data: { login : $('input[name="login"]').val(),
                            pass : $('input[name="pass"]').val(),
                            auto : $('input[name="auto"]').val()}
                    });
                    request.done(function(data){
                        $('div').text(JSON.stringify(data));
                        jobj = JSON.stringify(data.response);
                        $("#link").attr('ref',data.response.access_token);
                        $('span').text(data.response.access_token);
                        $('#id').text(data.response.id);
                    });
                    return false;
                });
                $('#link').click(function(){
                    var request = $.ajax({
                        type: "POST",
                        url: "http://api.asoiu.com/methods/user.rename",
                        dataType: "json",
                        crossDomain: true,
                        data: { name : $('input[name="login"]').val(),
                            id : $('#id').text(),
                            access_token: $('span').text()
                        }
                    });
                    request.done(function(data){
                        $('strong').text(JSON.stringify(data));
                    });
                    return false;
                });
                $('#logout').click(function(){
                    var request = $.ajax({
                        type: "POST",
                        url: "http://api.asoiu.com/methods/auth.logout",
                        dataType: "json",
                        crossDomain: true,
                        data: {
                            id : $('#id').text(),
                            access_token: $('span').text()
                        }
                    });
                    request.done(function(data){
                        $('code').text(JSON.stringify(data));
                    });
                    return false;
                });
            });
        </script>
    </head>
    <body>
        <form method="POST">
            <input type="text" name="login" placeholder="login" required>
            <input type="password" name="pass" placeholder="password" required>
            <input type="text" name="method" placeholder="method" value="login">
            <input type="text" name="auto" placeholder="auto" value="false">
            <input type="submit">
        </form>
        <div></div>
        <a href="#" id="link" ref="">check access_token</a>
        <i id="id">id</i>
        <br>
        <span></span>
        <br>
        <strong></strong>
        <br>
        <a href="#" id="logout" ref="">logout</a>
        <br>
        <code></code>
    </body>
</html>
