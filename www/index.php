<?php

require 'flight/Flight.php';

include_once "facade/APIAuth.php";
include_once "facade/APIUser.php";
include_once "facade/Messages.php";
include_once "facade/Documents.php";
include_once "facade/ITWork.php";

//error_reporting(0);
session_start();

$_SESSION['lang'] = empty($_SESSION['lang']) ? 'ua' : (empty($_GET['lang']) ? $_SESSION['lang'] : $_GET['lang']);

Flight::route('/', 'index');
Flight::route('/login', 'login');
Flight::route('/logout', 'logout');
Flight::route('/user/@id', 'userById');
Flight::route('/user/', 'users');
Flight::route('/add', 'add');
Flight::route('/rename', 'test');
Flight::route('/about', 'about');
Flight::route('/itworks/add/', 'itworksAdd');
Flight::route('/itworks/@id/edit/', 'itworksEdit');
Flight::route('/itworks/@id/project/', 'itworksAddProject');
Flight::route('/itworks/project/@id/worker/', 'itworksAddWorker');
Flight::route('/itworks/@id', 'itworksById');
Flight::route('/itworks/project/@id', 'projectById');
Flight::route('/itworks', 'itworks');
Flight::route('/documents', 'documents');
Flight::route('/documents/add/', 'documentsAdd');
Flight::route('/documents/@id/edit/', 'documentsEdit');
Flight::route('/documents/@id/delete/', 'documentsDelete');


Flight::start();

function lang($ua, $ru) {
    return $_SESSION['lang'] == 'ua' ? $ua : $ru;
}

function isOnline() {
    return !empty($_SESSION['access_token']);
}

function user($id) {
    if ($id == 'id')
        return $_SESSION['user_id'];
    if (isOnline())
        return APIUser::Get(array('id' => $_SESSION['user_id']));
}

function index($id) {
    if(isOnline())
        $id = user('id');
    include_once('templates/header.php');
    if(isOnline())
        include('templates/userById.php');
    else
        include('templates/login.php');
    readfile('templates/footer.html');
}

function resolved() {
    include_once('templates/header.php');
    include('templates/resolved.php');
    readfile('templates/footer.html');
}

function issueById($id) {
    include_once('templates/header.php');
    include('templates/issuebyid.php');
    readfile('templates/footer.html');
}

function userById($id) {
    include_once('templates/header.php');
    include('templates/userbyid.php');
    readfile('templates/footer.html');
}

function itworksById($id) {
    include_once('templates/header.php');
    include('templates/itworksbyid.php');
    readfile('templates/footer.html');
}

function itworksAdd() {
    include_once('templates/header.php');
    include('templates/itworksAdd.php');
    readfile('templates/footer.html');
}

function itworksEdit($id) {
    include_once('templates/header.php');
    include('templates/itworksEdit.php');
    readfile('templates/footer.html');
}

function itworksAddProject($id) {
    include_once('templates/header.php');
    include('templates/itworksProject.php');
    readfile('templates/footer.html');
}

function itworksAddWorker($id) {
    include_once('templates/header.php');
    include('templates/itworksWorker.php');
    readfile('templates/footer.html');
}

function projectById($id) {
    include_once('templates/header.php');
    include('templates/projectbyid.php');
    readfile('templates/footer.html');
}

function users() {
    include_once('templates/header.php');
    include('templates/users.php');
    readfile('templates/footer.html');
}

function add() {
    include_once('templates/header.php');
    include('templates/add.php');
    readfile('templates/footer.html');
}

function login() {
    include_once('templates/header.php');
    include('templates/login.php');
    readfile('templates/footer.html');
}

function logout() {
    unset($_SESSION['user_id']);
    unset($_SESSION['access_token']);
    header('Location: http://asoiu.com/');
    echo "<script>location.href='http://asoiu.com/'</script>";
}

function about() {
    include_once('templates/header.php');
    include('templates/about.php');
    readfile('templates/footer.html');
}

function test() {
    include_once('templates/header.php');
    include('templates/test.php');
    readfile('templates/footer.html');
}

function itworks() {
    include_once('templates/header.php');
    include('templates/itworks.php');
    readfile('templates/footer.html');
}

function documents() {
    include_once('templates/header.php');
    include('templates/documents.php');
    readfile('templates/footer.html');
}
function documentsAdd(){
    include_once('templates/header.php');
    include('templates/documentsAdd.php');
    readfile('templates/footer.html');
}

function documentsDelete($id){
    include_once('templates/header.php');
    include('templates/documentsDelete.php');
    readfile('templates/footer.html');
}

function documentsEdit($id){
    include_once('templates/header.php');
    include('templates/documentsEdit.php');
    readfile('templates/footer.html');
}

function prepareFileName($str) {
    $arr = array(
    'а' => 'a', 'б' => 'b', 'в' => 'v',
    'г' => 'g', 'д' => 'd', 'е' => 'e',
    'ё' => 'yo', 'ж' => 'zh', 'з' => 'z',
    'и' => 'i', 'й' => 'j', 'к' => 'k',
    'л' => 'l', 'м' => 'm', 'н' => 'n',
    'о' => 'o', 'п' => 'p', 'р' => 'r',
    'с' => 's', 'т' => 't', 'у' => 'u',
    'ф' => 'f', 'х' => 'x', 'ц' => 'c',
    'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh',
    'ь' => '\'', 'ы' => 'y', 'ъ' => '\'\'',
    'э' => 'e\'', 'ю' => 'yu', 'я' => 'ya',
    'А' => 'A', 'Б' => 'B', 'В' => 'V',
    'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
    'Ё' => 'YO', 'Ж' => 'Zh', 'З' => 'Z',
    'И' => 'I', 'Й' => 'J', 'К' => 'K',
    'Л' => 'L', 'М' => 'M', 'Н' => 'N',
    'О' => 'O', 'П' => 'P', 'Р' => 'R',
    'С' => 'S', 'Т' => 'T', 'У' => 'U',
    'Ф' => 'F', 'Х' => 'X', 'Ц' => 'C',
    'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH',
    'Ь' => '\'', 'Ы' => 'Y\'', 'Ъ' => '\'\'',
    'Э' => 'E\'', 'Ю' => 'YU', 'Я' => 'YA',
    ' ' => '-'
);
    return strtr($str, $arr);
}