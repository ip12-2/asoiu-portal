<?php

include_once "facade.php";

/**
 * Description of User
 *
 * @author AnarH
 */
class APIUser extends facade {

    public static function Get($var) {
        return self::Make("User", __FUNCTION__, $var);
    }

}

?>
