<?php

include_once "facade.php";

/**
 * Description of User
 *
 * @author AnarH
 */
class Documents extends facade {
    /**
     * (PHP 4, PHP 5)<br/>
     * Gets the array data of document
     * @link http://api.asoiu.com/docs/document
     * @param int $id <p>
     * The id of document.
     * </p>
     * @return array the value of the document data, including login of the responsible user, or Error object.
     */
     public static function Get($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
     public static function GetAll($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
    /**
     * (PHP 4, PHP 5)<br/>
     * Creates new document with specified parameters
     * @link http://api.asoiu.com/docs/document
     * @param string $name <p>
     * The name of the document
     * @param link $pic <p>
     * Link to the picture of the document
     * @param int $resp <p>
     * ID of the responsible user.
     * </p>
     * @return nothing.
     */
    public static function Create($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
    
    /**
     * (PHP 4, PHP 5)<br/>
     * Edits selected document
     * @link http://api.asoiu.com/docs/document
     * @param int $id <p>
     * ID of the document to edit
     * @param string $name <p>
     * New name of the document
     * @param link $pic <p>
     * New link to the picture of the document
     * @param bool $done <p>
     * 1 if document was processed, 0 if wasn't
     * @param int $resp <p>
     * ID of new responsible user.
     * </p>
     * @return nothing.
     */
    public static function Edit($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
    
    
    /**
     * (PHP 4, PHP 5)<br/>
     * Deletes selected document and its assignments
     * @link http://api.asoiu.com/docs/document
     * @param int $id <p>
     * ID of the document to delete
     * </p>
     * @return nothing.
     */
    public static function Delete($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
    
    /**
     * (PHP 4, PHP 5)<br/>
     * Assigns user to the document
     * @link http://api.asoiu.com/docs/document
     * @param int $doc <p>
     * ID of the assigning document
     * @param int $user <p>
     * ID of the user to assign
     * </p>
     * @return nothing.
     */
    public static function Assign($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    /**
     * (PHP 4, PHP 5)<br/>
     * Dessigns user from the document
     * @link http://api.asoiu.com/docs/document
     * @param int $doc <p>
     * ID of the deassigning document
     * @param int $user <p>
     * ID of the user to deassign
     * </p>
     * @return nothing.
     */
    public static function Dessign($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
}