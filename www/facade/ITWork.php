<?php

include_once "facade.php";

/**
 * Description of User
 *
 * @author AnarH
 */
class ITWork extends facade {

     public static function AddCompany($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
    public static function AddProject($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
    public static function AddProjectForStudent($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function EditCompany($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function EditProject($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function DeleteCompany($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function DeleteProject($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function DismissStudentFromProject($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function GetCompany($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function GetProject($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
    public static function GetCompanyProject($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function GetStudentProjects($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function GetStudentCompanies($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function GetAllCompany($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function GetAllProject($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }
    public static function GetCompanyStudents($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

    public static function GetProjectStudents($var) {
        return self::Make(__CLASS__,  __FUNCTION__, $var);
    }

}

?>
