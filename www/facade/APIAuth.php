<?php

include_once "facade.php";

/**
 * Description of Auth
 *
 * @author AnarH
 */
class APIAuth extends facade {

    public static function Login($var) {
        return self::Make("Auth", __FUNCTION__, $var);
    }

    public static function Signup($var) {
        return self::Make("Auth", __FUNCTION__, $var);
    }
    
    public static function SocialLogin($var) {
        return self::Make("Auth", __FUNCTION__, $var);
    }
    
    public static function Restore($var) {
        return self::Make("Auth", __FUNCTION__, $var);
    }

    public static function Logout($var) {
        return self::Make("Auth", __FUNCTION__, $var);
    }

}

?>
