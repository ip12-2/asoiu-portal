<?php

/**
 * Description of facade
 *
 * @author AnarH
 */
include_once "../api/engine/Response.php";
include_once "../api/engine/db_config.php";
include_once "../api/module/Auth/Auth.php";

class facade {

    public static function Make($class, $method, $var) {

        error_reporting(E_ERROR | E_PARSE);

        $r = null;
        $var['access_token'] = $_SESSION['access_token'];

        if ($class == "Auth") {
            $r = Auth::$method($var);
        } else {
            $moduleInterface = $class . "Interface";
            include_once "../api/module/" . strtolower($class) . "/" . "factory" . ".php";
            $classname = "API" . $class . "V" . $moduleInterface::version;


            $object = new $classname;
            $apiobject = $object->APIMethod();

            $rights = $apiobject::$rights;
            $authID = Auth::CheckRights($rights[strtolower($method)], $var['access_token'], $_SERVER['REMOTE_ADR']);
            
            if (get_class($authID) == "Error") {
                $err = $authID->GetError();
                if ($err['code'] == -20) {
                    header("Location: http://www.asoiu.com/error");
                }
            } elseif ($authID == 1) {
                $r = $apiobject::$method($var);
            } elseif ($authID == 0 || $authID == -2) {
                header("Location: http://www.asoiu.com/login");
            } elseif ($authID == -1) {
                header("Location: http://www.asoiu.com/access_denied");
            } elseif ($authID == -3) {
                header("Location: http://www.asoiu.com/error");
            }
        }

        $res = new Response($r);

        $result = $res->GetResponse("object");

        return $result->error ? $result : $result->response;
    }

}

?>
