<?php

/**
 * Description of Response
 *
 * @author AnarH
 * @since 28.02.14 11:02
 */
include_once "Error.php";

class Response {

    private $arr;
    private $type;

    function __construct($class) {
        if (is_array($class) || is_string($class)) {
            $this->type = "response";
            $this->arr = $class;
        } else
        if (get_class($class) == "Error") {
            $this->type = "error";
            $this->arr = $class->GetError();
        }
    }

    function GetResponse($type) {
        $type = $type != null ? $type : "json"; //set default return type;
        $arr = array($this->type => $this->arr);
        $result;
        switch ($type) {
            case "json":
                $result = json_encode($arr);
                break;
            case "xml":

                function array_to_xml(array $arr, SimpleXMLElement $xml) {
                    foreach ($arr as $k => $v) {
                        is_array($v) ? array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
                    }
                    return $xml;
                }

                $result = array_to_xml($arr, new SimpleXMLElement('<root/>'))->asXML();
                break;
            case "array":
                $result = print_r($arr, TRUE);
                break;
            case "object":
                $result = json_decode(json_encode($arr), FALSE);
                //$err = new Error(200, "wrong type of return value", "http://api.asoiu.com/doc/api-return-type");
                //$result = json_decode(json_encode(array("error" => $err->GetError())));
                break;
            default:
                $err = new Error(200, "wrong type of return value", "http://api.asoiu.com/doc/api-return-type");
                $result = json_encode(array("error" => $err->GetError()));
                break;
        }
        return $result;
    }

}

?>
