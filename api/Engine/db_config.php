<?php

/**
 * Description of db_config
 *
 * @author AnarH
 * @since 09.03.14 16:47
 *
 */
class db_config {

    public $error;
    public $result;
    public $STH;
    private $DBH;
    private static $instance;

    private function __construct() {
        try {
            /*$host = "localhost";
            $dbname = "api_asoiu";
            $user = "root";
            $pass = "";*/
            $host = "sql4.freesqldatabase.com";
            $dbname = "sql434456";
            $user = "sql434456";
            $pass = "eV7%rC5!";
            //$driver_options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET names = 'utf-8', lc_time_names = 'ru_RU',time_zone = 'Europe/Moscow'");
            $this->DBH = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $pass);
            $this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            //TODO: получить имя метода из которого вызван метод
            file_put_contents('engine/PDOErrors.txt', date("d.m.Y H:i:s") . " | " . __METHOD__ . " | " . $e->getMessage() . "\n\n", FILE_APPEND);
            $this->error = new Error(-20, "Can't connect to DB! plz, write to support", "http://api.asoiu.com/support");
        }
    }

    private function __clone() { /* ... @return Singleton */
    }

    private function __wakeup() { /* ... @return Singleton */
    }

    public static function getInstance() {    // @return Singleton
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __destruct() {
        $this->DBH = null;
    }

    public function query($query, $bind, $error, $data, $fetch = PDO::FETCH_OBJ) {
        try {
            $this->STH = $this->DBH->prepare($query);
            $bind($this->STH);
            $this->STH->execute($data);
            $this->result = $this->DBH->lastInsertId();
            $this->STH->setFetchMode($fetch);
        } catch (PDOException $e) {
            //TODO: получить имя метода из которого вызван метод
            file_put_contents('engine/PDOErrors.txt', date("d.m.Y H:i:s") . " | " . __METHOD__ . " | " . "var_dump(debug_backtrace())" . " | " . $e->getMessage() . "\n\n", FILE_APPEND);

            $this->error = $error(new Error(200, "Something way wrong! plz, write to support", "http://api.asoiu.com/support"));
        }
    }

}

?>