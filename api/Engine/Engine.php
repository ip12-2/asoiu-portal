<?php
/**
 * Description of Engine
 *
 * @author AnarH
 * @since 08.03.14 17:12
 *
 */

include "router.php";
include "Response.php";
include "db_config.php";

class Engine {
    function __construct() {

        error_reporting(E_ERROR | E_PARSE);

        $router = new Router();
        $result = $router->start();

        $res = new Response($result);
        print_r($res->GetResponse($router->res_type));
        
    }

}