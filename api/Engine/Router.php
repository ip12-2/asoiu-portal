<?php

include_once 'Error.php';

class Router {

    public $res_type = null;

    function start() {

        $_RQDATA = $_SERVER['REQUEST_METHOD'] == "GET" ? $_GET : $_POST;
        $str = $_SERVER['REDIRECT_URL'];
        $methods = substr($str, 1, 7);
        if ($methods != "methods") {
            $err = new Error(200, "invalide api request");
            return $err;
        }
        if (strpos($str, '.') <= 0) {
            $err = new Error(200, "method name is empty");
            return $err;
        }
        $module = substr($str, 9, strpos($str, '.') - strlen($methods) - 2);
        $sndd = strpos($str, '.', strpos($str, '.') + 1) - strpos($str, '.') - 1;
        if ($sndd <= 0) {
            $method = substr($str, strpos($str, '.') + 1);
        } else {
            $method = substr($str, strpos($str, '.') + 1, $sndd);
            $this->res_type = substr($str, strpos($str, '.') + strlen($method) + 2);
        }
        if (strlen($method) == 0) {
            $err = new Error(200, "method name is empty");
            return $err;
        }

        $api = $_RQDATA["v"];

        if (!empty($api) && floatval($api) == 0) {
            return new Error(200, "parrametr 'Version' is wrong!", "http://api.asoiu.com/doc/api-versions");
        }

        include_once ("module/Auth/Auth.php");
        if ($module == "auth") {
            $auth = new Auth();
            if ($method == "logout") {
                if (empty($_RQDATA['access_token']))
                    return new Error(200, "Access_token is empty");
                if ($auth->CheckAccessToken($_RQDATA['access_token'], 10, $_SERVER['REMOTE_ADR']) == -1)
                    return new Error(403, "Access denied", "http://api.asoiu.com/doc/modules_rules");
                elseif ($auth->CheckAccessToken($_RQDATA['access_token'], 10, $_SERVER['REMOTE_ADR']) == 1)
                    return $auth->$method($_RQDATA);
                elseif ($auth->CheckAccessToken($_RQDATA['access_token'], 10, $_SERVER['REMOTE_ADR']) == 0)
                    return new Error(403, "Access token is't correct");
            }
            if (method_exists($auth, $method)) {
                return $auth->$method($_RQDATA);
            } else {
                return new Error(200, "Method is not exists", "http://api.asoiu.com/doc/modules");
            }
        }

        if (!file_exists("module/" . $module . "/factory.php")) {
            return new Error(200, "Module is not exists", "http://api.asoiu.com/doc/modules");
        }
        include_once ("module/" . $module . "/factory.php");

        $moduleInterface = $module . "Interface";
        if (empty($api)) {
            $api = $moduleInterface::version;
        }
        $api = str_replace(array('.', ','), "_", $api);
        if ($api == $moduleInterface::version) {
            $apiclass = "API" . $module . "V" . $api;
            $apimethods = new $apiclass;
            $apiobject = $apimethods->APIMethod();

            if (method_exists($apiobject, $method)) {
                //check auth rules
                $auth = new Auth();
                $rights = $apiobject::$rights;
                $authID = $auth->CheckRights($rights[$method], $_RQDATA['access_token'], $_SERVER['REMOTE_ADR']);

                if (get_class($authID) == "Error") {
                    return $authID;
                } elseif ($authID == 1) {
                    return $apiobject->$method($_RQDATA);
                } elseif ($authID == 0) {
                    return new Error(403, "Access token empty", "http://api.asoiu.com/doc/modules_rules");
                } elseif ($authID == -1) {
                    return new Error(403, "Access denied", "http://api.asoiu.com/doc/modules_rules");
                } elseif ($authID == -2) {
                    return new Error(403, "Access token is't correct");
                } elseif ($authID == -3) {
                    return new Error(403, "Error in access right rules, plz contact with dev.support", "http://api.asoiu.com/help");
                }
            } else {
                return new Error(200, "Method is not exists", "http://api.asoiu.com/doc/modules");
            }
        } else {
            return new Error(200, "latest stable version is: " . str_replace("_", ".", $moduleInterface::version));
        }
        return;
    }

}

