<?php

/**
 * Description of Error
 *
 * @author AnarH
 * @since 28.02.14 10:28
 *
 */
class Error {

    private $code;
    private $message;
    private $link;
	
    function __construct($code, $message, $link = "http://api.asoiu.com/docs") {
        $this->code = $code;
        $this->message = $message;
        $this->link = $link;
    }

    function GetError(){
        return array("code" => $this->code, "message" => $this->message, "link" => $this->link);
    }

}

?>
