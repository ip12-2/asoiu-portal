<?php

include_once "../api/Engine/API.php";
include_once "MessagesV0_1.php";
include_once "MessagesV0_5.php";

class APIMessagesV0_1 implements API {

    public function APIMethod() {
        return new MessagesV0_1();
    }

}

class APIMessagesV0_5 implements API {

    public function APIMethod() {
        return new MessagesV0_5();
    }

}