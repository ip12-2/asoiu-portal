<?php

/**
 * Description of MessagesV0_5
 *
 * @author AnarH
 * @since 12.03.14 10:11
 *
 */

include_once 'MessagesInterface.php';

class MessagesV0_5  implements MessagesInterface {

    public static $rights = array("get" => 10, "send" => 0);

    public static function Get($var) {
        return "Get ProductB";
    }

    public static function Send($var) {
        return "Rename ProductB " . $var['id'];
    }

}
?>
