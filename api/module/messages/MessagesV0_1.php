<?php

/**
 * Description of MessagesV0_1
 *
 * @author AnarH
 * @since 12.03.14 01:10
 * 
 */
include_once 'MessagesInterface.php';

class MessagesV0_1 implements MessagesInterface {

    public static $rights = array("get" => 10, "send" => 5);

    /**
     * (PHP 4, PHP 5)<br/>
     * Gets the array data of user
     * @link http://api.asoiu.com/docs/user
     * @param int $id <p>
     * The id of user.
     * @param string $name <p>
     * The name of user.
     * @param link $photo <p>
     * The photo of user
     * </p>
     * @return array the value of the user data, or new Error() on an error.
     */
    public static function Get($var) {

        $array = array("name" => $var['name'], "id" => $var['id'], "text" => $var['text']);
        
        return $array;
    }

    public static function Send($var) {
        $array = array("name" => $var['name'], "id" => $var['id'], "text" => $var['text']);
        return $array;
    }

}

?>
