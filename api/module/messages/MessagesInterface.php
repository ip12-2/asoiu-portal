<?php

/**
 *
 * @author AnarH
 * @since 12.03.14 01:08
 *
 */
interface MessagesInterface {
    const version = "0_1";
    const isAT = true; //is need access_token;

    public static function Get($arr);

    public static function Send($arr);
}

?>
