<?php

/**
 *
 * @author AnarH
 * @since 28.02.14 10:00
 *
 */
interface ITWorkInterface {
    const version = "0_1";
    const isAT = true; //is need access_token;

    public static function AddCompany($var);
    
    public static function AddProject($var);
    
    public static function AddProjectForStudent($var);
    
    public static function EditCompany($var);
    
    public static function EditProject($var);
    
    public static function DeleteCompany($var);
    
    public static function DeleteProject($var);
    
    public static function DismissStudentFromProject($var);
    
    public static function GetCompany($var);
    
    public static function GetProject($var);
    
    public static function GetStudentProjects($var);
    
    public static function GetStudentCompanies($var);
}

?>
