<?php

/**
 * Description of UserV0_1
 *
 * @author Alex Belenko & Svityashchuk Oleksiy
 * @since 28.02.14 10:00
 * 
 */
include_once 'ITWorkInterface.php';

class ITWorkV0_1 implements ITWorkInterface {

    public static $rights = array(
        "addcompany" => 9,
        "editcompany" => 9,
        "addproject" => 9,
        "editproject" =>9,
        "addprojectforstudent" => 9,
        "deletecompany" => 9,
        "deleteproject" => 9,
        "getcompany" => 10,
        "getproject" => 10,
        "getcompanyproject" => 10,
        "getstudentprojects" => 10,
        "getallcompany" => 10,
        "getallproject" => 10,
        "getstudentcompanies" => 10,
        "getcompanystudents" => 10,
        "getprojectstudents" => 10);

    /**
     * (PHP 4, PHP 5)<br/>
     * Gets the array data of user
     * @link http://api.asoiu.com/docs/user
     * @param int $id <p>
     * The id of user.
     * @param string $name <p>
     * The name of user.
     * @param link $photo <p>
     * The photo of user
     * </p>
     * @return array the value of the user data, or new Error() on an error.
     */
      public static function AddCompany($var) {

        $query = "INSERT INTO itwork_companies  (company_name, founder_id, photo_url, company_url,"
                . "address, foundation_date, contract_url, is_contract_verified)"
                . "VALUES (:company_name, " . intval($var['founder_id']) . ", :photo_url, :company_url,"
                . " :address, " . intval($var['foundation_date']) . ", :contract_url, " . intval($var['is_contract_verified']).")";
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('company_name' => $var['company_name'],
            'photo_url' => $var['photo_url'],
            'company_url' => $var['company_url'],
            'address' => $var['address'],
            'contract_url' => $var['contract_url']));

        if ($result->error != null)
            return $result->error;


        return array("id" => $result->result);
    }

    public static function AddProject($var) {
         $query = "INSERT INTO itwork_projects  (company_id, project_name, photo_url, project_url, start_date, end_date)"
                . "VALUES (".  intval($var['company_id']).", :project_name, :photo_url, :project_url," . intval($var['start_date']) . "," . intval($var['end_date']) .");";
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('project_name' => $var['project_name'],
            'photo_url' => $var['photo_url'],
            'project_url' => $var['project_url']));

        if ($result->error != null)
            return $result->error;

        return array("id" => $result->result);
    }

    public static function AddProjectForStudent($var) {
        $query = "INSERT INTO itwork_students (student_id, project_id, company_id, date_of_recruitment, date_of_dismissal)"
                ."VALUES ("
                .intval($var['student_id'])
                .",". intval($var['project_id'])
                .",". intval($var['company_id'])
                .",". intval($var['date_of_recruitment'])
                .",". intval($var['date_of_dismissal'])
                .");";
       
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;  });

        if ($result->error != null)
            return $result->error;
        return array("id" => $result->result);
    }

    public static function EditCompany($var) {
        $query = "UPDATE itwork_companies "
                . "SET company_name = :company_name, "
                . "founder_id = ".intval($var['founder_id']).", "
                . "photo_url = :photo_url, "
                . "company_url = :company_url, "
                . "address = :address, "
                . "foundation_date = ".intval($var['foundayion_date']).", "
                . "contract_url = :contract_url, "
                . "is_contract_verified = ".intval($var['is_contract_verified'])." "
                ."WHERE id = ".intval($var['id']).";";
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('company_name' => $var['company_name'],
            'photo_url' => $var['photo_url'],
            'company_url' => $var['company_url'],
            'photo_url' => $var['photo_url'],
            'address' => $var['address'],
            'contract_url' => $var['contract_url']));
        if ($result->error != null)
            return $result->error;
        
        return array("code" => 1);
    }

    public static function EditProject($var) {
        $query = "UPDATE itwork_projects "
                . "SET project_name = :project_name, "
                . "photo_url = :photo_url, "
                . "project_url = :project_url, "
                . "start_date = ".intval($var['start_date']).", "
                . "end_date = ".intval($var['end_date'])." "
                . "WHERE id=".intval($var['id']).";";
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('project_name' => $var['project_name'],
            'photo_url' => $var['photo_url'],
            'project_url' => $var['project_url']));

        if ($result->error != null)
            return $result->error;

        return array("code" => 1);
                
    }

    public static function DeleteCompany($var) {
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(200, "Id isn't correct");
        }
        
        $query = "DELETE FROM itwork_companies WHERE id = ".intval($var['id']).";";
        
        $result = db_config::getInstance();
        $result->query($query, function($STH){ }, function($err) { return $err; });

        if ($result->error != null)
            return $result->error;
        
        return array("code" => 1);
    }

    public static function DeleteProject($var) {
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(200, "Id isn't correct");
        }
        
        $query = "DELETE FROM itwork_projects WHERE id = ".intval($var['id']).";";
        
        $result = db_config::getInstance();
        $result->query($query, function($STH){ }, function($err) { return $err; });

        if ($result->error != null)
            return $result->error;
        
        return array("code" => 1);
    }

    public static function DismissStudentFromProject($var) {
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(200, "Id isn't correct");
        }
        
        $query = "DELETE FROM itwork_students WHERE project_id = ".intval($var['project_id'])
                ."AND student_id = ".intval($var['student_id']).";";
        
        $result = db_config::getInstance();
        $result->query($query, function($STH){ }, function($err) { return $err; });

        if ($result->error != null)
            return $result->error;
        //Шо тут в STH писати? Як перевірити чи нормально всьо видалило.
        //Я не знаю чи я це правильно написав, оцей ретюрн.
        return array("code" => 1);
    }

    public static function GetCompany($var) {
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(200, "Id isn't correct");
        }
        $query = "SELECT * FROM itwork_companies WHERE id = ".intval($var['id']).";" ;
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        });

        if ($result->error != null)
            return $result->error;
        if ($result->STH->rowCount() == 0) {
            return new Error(200, "Company not found", "http://api.asoiu.com/docs/");
        }
        

        $company = $result->STH->fetchObject();
        return array(
            "id"=> $company->id,
            "company_name"=> $company->company_name, 
            "founder_id"=> $company->founder_id,
            "photo_url"=> $company->photo_url,
            "company_url"=> $company->company_url,
            "address"=> $company->address,
            "foundation_date"=> $company->foundation_date,
            "contract_url"=> $company->contract_url,
            "is_contract_verified"=> $company->is_contract_verified);
    }

    public static function GetProject($var) {
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(200, "Id isn't correct");
        }

        $query = "SELECT * FROM itwork_projects WHERE id = " . intval($var['id']) . ";";
        $result = db_config::getInstance();
        $result->query($query, function($STH){ }, function($err) { return $err; });

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0) {
            return new Error(200, "Project not found", "http://api.asoiu.com/docs/");
        }

        $project = $result->STH->fetchObject();
        return array(
            "id"=> $project->id, 
            "company_id" => $project->company_id,
            "project_name"=> $project->project_name,
            "photo_url"=> $project->photo_url,
            "project_url"=> $project->project_url,
            "start_date"=> $project->start_date,            
            "end_date"=> $project->end_date
            );
    }
    public static function GetCompanyProject($var) {
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(200, "Id isn't correct");
        }

        $query = "SELECT * FROM itwork_projects WHERE company_id = " . intval($var['id']) . ";";
        $result = db_config::getInstance();
        $result->query($query, function($STH){ }, function($err) { return $err; });

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0) {
            return new Error(200, "Project not found", "http://api.asoiu.com/docs/");
        }

        $res = array();
        for ($i = 0; $i < $result->STH->rowCount(); $i++) {
            $project = $result->STH->fetchObject();
            $res[$i]= array(
                        "id"=> $project->id, 
                        "company_id" => $project->company_id,
                        "project_name"=> $project->project_name,
                        "photo_url"=> $project->photo_url,
                        "project_url"=> $project->project_url,
                        "start_date"=> $project->start_date,            
                        "end_date"=> $project->end_date
                        );
        }
        return $res;
    }


    public static function GetStudentProjects($var) {
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(200, "Id isn't correct");
        }

        $query = "SELECT * FROM itwork_projects "
                ."WHERE id IN (SELECT project_id FROM itwork_students WHERE student_id = ".intval($var['id']).");";
//щоб тобі гарно спалося із цим запросом=)
//        
//print_r($query);
        $result = db_config::getInstance();
        $result->query($query, function($STH){ }, function($err) { return $err; });

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0) {
            return new Error(200, "Project not found", "http://api.asoiu.com/docs/");
        }

        $res = array();
        for ($i = 0; $i < $result->STH->rowCount(); $i++) {
            $project = $result->STH->fetchObject();
            $res[$i]= array(
                        "id"=> $project->id, 
                        "company_id" => $project->company_id,
                        "project_name"=> $project->project_name,
                        "photo_url"=> $project->photo_url,
                        "project_url"=> $project->project_url,
                        "start_date"=> $project->start_date,            
                        "end_date"=> $project->end_date
                        );
        }
        return $res;

    }

    public static function GetStudentCompanies($var) {
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(200, "Id isn't correct");
        }

        $query = "SELECT * FROM itwork_companies "
                . "WHERE id IN (SELECT company_id FROM itwork_projects "
                    . "WHERE id IN (SELECT project_id FROM itwork_students WHERE student_id = ".intval($var['id'])
                    .")"
                . ") OR founder_id = ".intval($var['id']);
        $result = db_config::getInstance();
        $result->query($query, function($STH){ }, function($err) { return $err; });

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0) {
            return new Error(200, "Companies not found", "http://api.asoiu.com/docs/");
        }

        $res = array();
        for ($i = 0; $i < $result->STH->rowCount(); $i++) {
            $company = $result->STH->fetchObject();
            $res[$i]= array(
                        "id"=> $company->id, 
                        "company_name" => $company->company_name,
                        "founder_id"=> $company->founder_id,
                        "photo_url"=> $company->photo_url,
                        "company_url"=> $company->company_url,
                        "address"=> $company->address,            
                        "foundation_date"=> $company->foundation_date,
                        "contract_url"=>$company->contract_url,
                        "is_contract_verified"=>$company->is_contract_verified
                        );
        }
        return $res;
    }

    public static function GetAllCompany($var) {
        $query = "SELECT * FROM itwork_companies;";
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        });

        if ($result->error != null)
            return $result->error;
        if ($result->STH->rowCount() == 0) {
            return new Error(200, "Company not found", "http://api.asoiu.com/docs/");
        }
        

        $res = array();
        for ($i = 0; $i < $result->STH->rowCount(); $i++) {
            $company = $result->STH->fetchObject();
            $res[$i] = array(
                "id"=> $company->id,
                "company_name"=> $company->company_name, 
                "founder_id"=> $company->founder_id,
                "photo_url"=> $company->photo_url,
                "company_url"=> $company->company_url,
                "address"=> $company->address,
                "foundation_date"=> $company->foundation_date,
                "contract_url"=> $company->contract_url,
                "is_contract_verified"=> $company->is_contract_verified);
        }
        return $res;
    }
    
    public static function GetCompanyStudents($var) {
        $query = "SELECT user.id, user_fields.first_name,user_fields.last_name,user_fields.photo_url
                  FROM  user, user_fields
                  WHERE user.id = user_fields.uid AND user.id IN (SELECT student_id FROM itwork_students WHERE company_id = ".$var['id'].")";
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        });

        if ($result->error != null)
            return $result->error;
        if ($result->STH->rowCount() == 0) {
            return new Error(200, "Company not found", "http://api.asoiu.com/docs/");
        }
        

        $res = array();
        for ($i = 0; $i < $result->STH->rowCount(); $i++) {
            $company = $result->STH->fetchObject();
            $res[$i] = array(
                "id"=> $company->id,
                "first_name"=> $company->first_name, 
                "last_name"=> $company->last_name,
                "photo_url"=> $company->photo_url);
        }
        return $res;
    }
    public static function GetProjectStudents($var) {
        $query = "SELECT user.id, user_fields.first_name,user_fields.last_name,user_fields.photo_url
                  FROM  user, user_fields
                  WHERE user.id = user_fields.uid AND user.id IN (SELECT student_id FROM itwork_students WHERE project_id = ".$var['id'].")";
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        });

        if ($result->error != null)
            return $result->error;
        if ($result->STH->rowCount() == 0) {
            return new Error(200, "Company not found", "http://api.asoiu.com/docs/");
        }
        

        $res = array();
        for ($i = 0; $i < $result->STH->rowCount(); $i++) {
            $company = $result->STH->fetchObject();
            $res[$i] = array(
                "id"=> $company->id,
                "first_name"=> $company->first_name, 
                "last_name"=> $company->last_name,
                "photo_url"=> $company->photo_url);
        }
        return $res;
    }
    
}

?>
