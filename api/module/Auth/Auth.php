<?php

//TODO: класс должен быть статическим(?!)
/**
 * Description of Auth
 *
 * @author AnarH
 * @since 04.03.14 15:42
 *
 */
class Auth {

    public static function Login($var) {
        if (self::CheckLogin($var['login']) == 0) {
            return new Error(1, "Login field is empty");
        } elseif (self::CheckLogin($var['login']) == -1) {
            return new Error(2, "Login must be less than 64 chars");
        }
        if (!self::CheckPass($var['pass'])) {
            return new Error(3, "Password must be more than 6 chars");
        }

        $query = "SELECT id,role,salt,pass FROM  user WHERE  login = :login";
        $result = db_config::getInstance();
        if ($result->error != null)
            return $result->error;
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('login' => $var['login']));

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0) {
            return new Error(4, "User not find", "http://api.asoiu.com/docs/");
        }

        $user = $result->STH->fetchObject();

        $id = $user->id;
        $role = $user->role;
        $salt = $user->salt;
        $md5DB = $user->pass;
        $checkmd5 = md5(md5($var['pass']) . $salt);

        if ($checkmd5 != $md5DB) {
            return new Error(5, "Password is't correct");
        }

        $at = self::GenerateAccessToken($id, $_SERVER['REMOTE_ADR'], $role);
        if (get_class($at) == "Error")
            return $at;

        $query = "UPDATE user SET lastactive = " . time() . " WHERE id = :id";
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('id' => $id));

        if ($result->error != null)
            return $result->error;

        $result = array("access_token" => $at['md5'], "time" => $at['time'], "id" => $id);

        return $result;
    }

    public static function Signup($var) {
        if (self::CheckLogin($var['login']) == 0) {
            return new Error(1, "Login field is empty");
        } elseif (self::CheckLogin($var['login']) == -1) {
            return new Error(2, "Login must be less than 64 chars");
        }
        if (!self::CheckPass($var['pass'])) {
            return new Error(3, "Password must be less than 6 chars");
        }

        $query = "SELECT * FROM  user WHERE  login = :login";
        $result = db_config::getInstance();
        if ($result->error != null)
            return $result->error;
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('login' => $var['login']));

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() > 0)
            return new Error(4, "This login is already register!", "http://asoiu.com/login");

        $salt = self::MakeSalt();
        $md5 = md5(md5($var['pass']) . $salt);

        $online = ($var['auto'] == "true") ? 1 : 0;
        $data = array('login' => $var['login'], 'pass' => $md5, 'salt' => $salt);
        $query = "INSERT INTO user (login, pass, salt)
            VALUES (:login, :pass, :salt)";
        $result->query($query, function($STH) {
            $STH->bindParam(':login', $var['login']);
            $STH->bindParam(':pass', $md5, PDO::PARAM_STR);
            $STH->bindParam(':salt', $salt, PDO::PARAM_STR);
            return $STH;
        }, function($err) {
            return $err;
        }, $data);

        if ($result->error != null)
            return $result->error;

        $res = null;
        if ($var['auto'] == "true") {
            $at = self::GenerateAccessToken($result->result, $_SERVER['REMOTE_ADR'], 10);
            if (get_class($at) == "Error")
                return $at;
            $res = array("access_token" => $at['md5'], "time" => $at['time'], "id" => $result->result);
        } else {
            $res = array("id" => $result->result);
        }

        return $res;
    }

    public static function SocialLogin($var) {
        if (empty($var['type']))
            return new Error(200, "Type is empty");
        if (intval($var['type']) == 0)
            return new Error(200, "Type is wrong");
        if (empty($var['sid']))
            return new Error(200, "Sid is empty");
        if (intval($var['sid']) == 0)
            return new Error(200, "Sid is wrong");
        if (empty($var['login'])) {
            switch (intval($var['type'])) {
                case 1 : $var['login'] = "vk";
                    break;
                case 2 : $var['login'] = "fb";
                    break;
                case 3 : $var['login'] = "tw";
                    break;
                case 4 : $var['login'] = "gl";
                    break;
                default : $var['login'] = "null";
            }
            $var['login'] = $var['login'] . $var['sid'];
        }

        $query = "SELECT user.id, user.role FROM user_social, user WHERE user_social.type = :type AND user_social.sid = :sid AND user.id = user_social.uid";
        $result = db_config::getInstance();

        if ($result->error != null)
            return $result->error;
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('type' => $var['type'], 'sid' => $var['sid']));

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() > 0) {

            $user = $result->STH->fetchObject();

            $at = self::GenerateAccessToken($user->id, $_SERVER['REMOTE_ADR'], $user->role);
            if (!is_array($at) && get_class($at) == "Error")
                return $at;

            $query = "UPDATE user SET lastactive = " . time() . " WHERE id = :id";
            $result->query($query, function($STH) {
                
            }, function($err) {
                return $err;
            }, array('id' => $user->id));

            if ($result->error != null)
                return $result->error;

            return array("access_token" => $at['md5'], "time" => $at['time'], "id" => $user->id);
        } else {
            $salt = self::MakeSalt();
            $md5 = md5(md5($var['login']) . $salt);
            $query = "INSERT INTO user (login, pass, salt)
            VALUES (:login, :pass, :salt)";
            $result->query($query, function($STH) {
                return $STH;
            }, function($err) {
                return $err;
            }, array('login' => $var['login'], 'pass' => $md5, 'salt' => $salt));

            if ($result->error != null)
                return $result->error;

            $user_id = $result->result;

            $query = "INSERT INTO user_social (uid, type, sid)
            VALUES (:uid, :type, :sid)";
            $result->query($query, function($STH) {
                return $STH;
            }, function($err) {
                return $err;
            }, array('uid' => $user_id, 'type' => $var['type'], 'sid' => $var['sid']));

            if ($result->error != null)
                return $result->error;

            $query = "INSERT INTO user_fields (uid, first_name, last_name, photo_url)
            VALUES (:uid, :first_name, :last_name, :photo_url)";
            $result->query($query, function($STH) {
                return $STH;
            }, function($err) {
                return $err;
            }, array('uid' => $user_id, 'first_name' => empty($var['first_name']) ? '' : $var['first_name'], 'last_name' => empty($var['last_name']) ? '' : $var['last_name'], 'photo_url' => empty($var['photo_url']) ? '' : $var['photo_url']));

            if ($result->error != null)
                return $result->error;

            return array('id' => $user_id);
        }
    }

    public static function Restore($var) {
        if (empty($var['hash'])) {
            if (self::CheckLogin($var['login']) == 0) {
                return new Error(200, "Login field is empty");
            } elseif (self::CheckLogin($var['login']) == -1) {
                return new Error(200, "Login must be less than 64 chars");
            }

            $query = "SELECT id,role,salt,pass,login FROM  user WHERE  login = :login";
            $result = db_config::getInstance();
            if ($result->error != null)
                return $result->error;
            $result->query($query, function($STH) {
                
            }, function($err) {
                return $err;
            }, array('login' => $var['login']));

            if ($result->error != null)
                return $result->error;

            if ($result->STH->rowCount() == 0) {
                return new Error(200, "User not find", "http://api.asoiu.com/docs/");
            }

            $user = $result->STH->fetchObject();
            $id = $user->id;
            $role = $user->role;
            $mail = $user->login;
            $date = time(date('H') + 2, date('i'), date('s'), date('d'), date('m'), date('Y'));
            $hash = md5($date . $mail);
            #TODO: generate hash
            $link = "http://api.asoiu.com/methods/auth.restore?hash=" . $hash;

            $query = "INSERT INTO pass_restoration (hash, time, uid) VALUES(:hash,:time, :uid)";
            $result = db_config::getInstance();
            if ($result->error != null)
                return $result->error;
            $result->query($query, function($STH) {
                
            }, function($err) {
                return $err;
            }, array('hash' => $hash, 'time' => $date, 'uid' => $id));

            if ($result->error != null)
                return $result->error;

            if (mail($mail, "Восстановление пароля", "Братишка, ты пароль забыл. перейди по этой ссылке и будет тебе добро: " . $link))
                return array("id" => $id);
            else
                return new Error(200, "Can't send email, plz contact to support", "http://api.asoiu.com/support");
        }else {
            #TODO: check hash
            $query = "SELECT user.id, user.login, pass_restoration.*  FROM  pass_restoration, user WHERE  hash = :hash AND pass_restoration.uid =user.id ORDER BY user.id DESC LIMIT 0 , 1";
            $result = db_config::getInstance();
            if ($result->error != null)
                return $result->error;
            $result->query($query, function($STH) {
                
            }, function($err) {
                return $err;
            }, array('hash' => $var['hash']));

            if ($result->error != null)
                return $result->error;

            if ($result->STH->rowCount() == 0) {
                return new Error(200, "hash is empty", "http://api.asoiu.com/docs/");
            }
            $res = $result->STH->fetchObject();

            if ($res->time < time()) {
                return new Error(200, "hash is out of date", "http://api.asoiu.com/docs/");
            }
            if (md5($res->time . $res->login) == $res->hash) {
                return array("hash" => "ok");
            } else {
                return new Error(200, "hash isn`t correct", "http://api.asoiu.com/docs/");
            }
        }
    }

    public static function Logout($var) {
        if (!empty($var['id']) && floatval($var['id']) == 0) {
            return new Error(200, "id field is empty");
        }

        $query = "SELECT online FROM  user WHERE  id = :id";
        $result = db_config::getInstance();
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('id' => $var['id']));

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0) {
            return new Error(200, "User not find");
        } else {
            $user = $result->STH->fetchObject();
            if ($user->online == 0) {
                return new Error(200, "User already offline");
            }
        }

        $query = "UPDATE user SET online = '0' WHERE id = :id";
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('id' => $var['id']));

        if ($result->error != null)
            return $result->error;

        return array("logout" => 1);
    }

    private static function MakeSalt() {
        mt_srand(microtime(true) + 100000 + memory_get_usage(true));
        return md5(uniqid(mt_rand(), true));
    }

    private static function CheckPass($pass) {
        if (empty($pass) || strlen($pass) < 6)
            return false;
        return true;
    }

    private static function CheckLogin($login) {
        if (empty($login))
            return 0;
        if (strlen($login) > 64)
            return -1;
        return 1;
    }

    public static function CheckRights($right, $access_token, $client) {
        if (!empty($access_token)) {
            $res = self::CheckAccessToken($access_token, $right, $client);
            if (get_class($res) == "Error")
                return $res;
            elseif ($res == 0)
                return -1;
            else
                return $res;
        }
        if ($right == 10)
            return 1;
        if ($right < 10 && empty($access_token))
            return 0;

        return -3;
    }

    private static function GenerateAccessToken($id, $client, $role) {
        $date = time(date('H'), date('i'), date('s'), date('d') + 1, date('m'), date('Y'));
        $md5 = md5($id . $client . $role . $date);

        $query = "SELECT access_token FROM access_token WHERE uid = :id";
        $result = db_config::getInstance();
        if ($result->error != null)
            return $result->error;
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('id' => $id));

        if ($result->error != null)
            return $result->error;
//echo $result->STH->rowCount().$md5;
        if ($result->STH->rowCount() > 0) {
            $data = array('time' => $date, 'access_token' => $md5, 'uid' => $id);
            $query = "UPDATE access_token SET time = :time, access_token = :access_token WHERE uid = :uid";
            $result->query($query, function($STH) {
                
            }, function($err) {
                return $err;
            }, $data);

            if ($result->error != null)
                return $result->error;
        } else {
            $data = array('time' => $date, 'access_token' => $md5, 'uid' => $id);
            $query = "INSERT INTO access_token (time, access_token, uid) VALUES (:time, :access_token, :uid)";
            $result->query($query, function($STH) {
                
            }, function($err) {
                return $err;
            }, $data);

            if ($result->error != null)
                return $result->error;
        }
        return array("md5" => $md5, "time" => $date);
    }

    public static function CheckAccessToken($access_token, $right, $client) {
        $query = "SELECT access_token.access_token, access_token.uid, user.id, user.role, access_token.time
                    FROM access_token, user
                    WHERE access_token.access_token = :access_token AND access_token.uid = user.id";
        $result = db_config::getInstance();
        if ($result->error != null)
            return $result->error;
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('access_token' => $access_token));

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0) {
            return 0;
        } else {
            $at = $result->STH->fetchObject();
            $result->query("UPDATE user SET lastactive = :la WHERE id = :id", function($STH) {
//2014-03-31 06:33:28
            }, function($err) {
                return $err;
            }, array('la' => time(), 'id' => $at->id));

            if ($result->error != null)
                return $result->error;

            if ($at->role <= $right) {
                return md5($at->id . $client . $at->role . $at->time) == $access_token ? 1 : -1;
            } else {
                return new Error(200, "No access");
            }
        }
    }

    public static function GetCurrentUser($access_token) {

        if (empty($access_token))
            return new Error(200, "Access_token is empty");
        $query = "SELECT access_token.uid,access_token.time
                    FROM access_token
                    WHERE access_token.access_token = :access_token";
        $result = db_config::getInstance();
        if ($result->error != null)
            return $result->error;
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('access_token' => $access_token));

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0)
            return new Error(1, "Access token is't correct");

        $at = $result->STH->fetchObject();
        if (time() > $at->time)
            return new Error(2, "Access token is out of date");

        return $at->uid;
    }

}
