<?php

/**
 * Description of UserV0_1
 *
 * @author AnarH
 * @since 28.02.14 10:00
 * 
 */
include_once 'UserInterface.php';

class UserV0_1 implements UserInterface {

    public static $rights = array("get" => 10, "rename" => 9);

    /**
     * (PHP 4, PHP 5)<br/>
     * Gets the array data of user
     * @link http://api.asoiu.com/docs/user
     * @param int $id <p>
     * The id of user.
     * @param string $name <p>
     * The name of user.
     * @param link $photo <p>
     * The photo of user
     * </p>
     * @return array the value of the user data, or new Error() on an error.
     */
    public static function Get($var) {
        $query = "SELECT user.id,user.role,user.login,user.created,user.lastactive,
                        user_fields.first_name,user_fields.last_name,user_fields.birthday,user_fields.photo_url
                  FROM  user, user_fields
                  WHERE user.id = user_fields.uid";
        if (!empty($var['id'])) {
            if (intval($var['id']) == 0)
                return new Error(3, "id isn't correct");
            $query = $query . " AND user.id = :id";
        }else {
            if (!empty($var['role']))
                if ($var['role'] != 10)
                    $query = $query . " AND user.role = " . intval($var['role']);
            $limit = intval($var['limit']) == 0 ? 100 : intval($var['limit']);
            $offset = intval($var['offset']) == 0 ? 0 : intval($var['offset']);
            $query = $query . " LIMIT $offset, $limit";
        }
        $result = db_config::getInstance();
        if ($result->error != null)
            return $result->error;
        $result->query($query, function($STH) {
            
        }, function($err) {
            return $err;
        }, array('id' => $var['id']));

        if ($result->error != null)
            return $result->error;

        if ($result->STH->rowCount() == 0) {
            return new Error(4, "User not find", "http://api.asoiu.com/docs/");
        }
        if (!empty($var['id'])) {
            if (intval($var['id']) != 0) {
                $user = $result->STH->fetchObject();
                $online = mktime() - 60 * 20 > $user->lastactive ? 0 : 1;
                return array(
                    "id" => $user->id, "login" => $user->login,
                    "role" => $user->role, "online" => $online,
                    "first_name" => $user->first_name, "last_name" => $user->last_name,
                    "photo_url" => $user->photo_url, "birthday" => $user->birthday,
                    "created" => $user->created, "lastactive" => $user->lastactive);
            }
        } else {
            $res = array();
            for ($i = 0; $i < $result->STH->rowCount(); $i++) {
                $user = $result->STH->fetchObject();
                $online = mktime() - 60 * 20 > $user->lastactive ? 0 : 1;
                $res[$i] = array(
                    "id" => $user->id, "login" => $user->login,
                    "role" => $user->role, "online" => $online,
                    "first_name" => $user->first_name, "last_name" => $user->last_name,
                    "photo_url" => $user->photo_url, "birthday" => $user->birthday,
                    "created" => $user->created, "lastactive" => $user->lastactive);
            }
            return $res;
        }
    }

    public static function Rename($var) {
        $array = array("name" => $var['name'], "id" => $var['id'], "photo" => $var['photo']);
        return $array;
    }

}

?>
