<?php

/**
 * Description of UserV0_5
 *
 * @author AnarH
 * @since 28.02.14 10:00
 *
 */

include_once 'UserInterface.php';

class UserV0_5  implements UserInterface {

    public static $rights = array("get" => 10, "rename" => 0);

    public static function Get($var) {
        return "Get ProductB";
    }

    public static function Rename($var) {
        return "Rename ProductB " . $var['id'];
    }

}
?>
