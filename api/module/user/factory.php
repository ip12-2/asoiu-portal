<?php
include_once "../api/Engine/API.php";
include_once "UserV0_1.php";
include_once "UserV0_5.php";

class APIUserV0_1 implements API {

    public function APIMethod() {
        return new UserV0_1();
    }

}

class APIUserV0_5 implements API {

    public function APIMethod() {
        return new UserV0_5();
    }

}