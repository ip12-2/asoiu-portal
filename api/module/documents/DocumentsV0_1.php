<?php

/**
 * Description of DocumentsV0_1
 *
 * @author Variecs
 * @since 24.03.14 18:00
 * 
 * @author Tolik
 * @since 25.03.14 18:00
 */
include_once 'DocumentsInterface.php';

class DocumentsV0_1 implements DocumentsInterface {

    public static $rights = array(
        "get" => 8, 
        "getall" => 8, 
        "edit" => 8, 
        "create" => 8, 
        "delete"=>8, 
        "assign" => 8/*?*/, 
        "deassign" => 8,
        );

    /**
     * (PHP 4, PHP 5)<br/>
     * Gets the array data of document
     * @link http://api.asoiu.com/docs/document
     * @param int $id <p>
     * The id of document.
     * </p>
     * @return array the value of the document data, including login of the responsible user, or Error object.
     */
    public static function Get($var) {

        $query = "SELECT doc.* FROM doc_documents doc LEFT JOIN user ON doc.resp = user.id WHERE doc.id = :id";
        $db = db_config::getInstance();
        $db->query($query, function($STH) {
                    
                }, function($err) {
                    return $err;
                }, array('id' => $var['id']));
        $document = $db->STH->fetchObject();

        $result = array('name' => $document->name,  'pic' => $document->pic, 'done' => $document->done, 'resp' => $document->resp);
        return $result;
    }

    public static function GetAll($var) {
        $query = "SELECT doc.*, user.login, user_fields.first_name, user_fields.last_name FROM doc_documents doc LEFT JOIN user ON doc.resp = user.id LEFT JOIN user_fields ON doc.resp = user_fields.uid";
        $db = db_config::getInstance();
        $db->query($query, function($STH) {
                    
                }, function($err) {
                    return $err;
                }, null);

        $result = array();
        for ($i = 0; $i < $db->STH->rowCount(); $i++) {
            $document = $db->STH->fetchObject();
            $result[$i]= array(
                'id' => $document->id, 
                'name' => $document->name,  
                'pic' => $document->pic, 
                'done' => $document->done, 
                'uid' => $document->resp, 
                'first_name' => $document->first_name, 
                'last_name' => $document->last_name
                    );
        }
        return $result;
    }
    
    /**
     * (PHP 4, PHP 5)<br/>
     * Creates new document with specified parameters
     * @link http://api.asoiu.com/docs/document
     * @param string $name <p>
     * The name of the document
     * @param link $pic <p>
     * Link to the picture of the document
     * @param int $resp <p>
     * ID of the responsible user.
     * </p>
     * @return nothing.
     */
    public static function Create($var) {
        $db = db_config::getInstance();

        if (self::CheckName($var['name'])){
            return new Error(500,"User name is empty or too long");
        }
        if(self::CheckLink($var['pic'])){
            return new Error(501,"Link is empty or too long");
        }

        $data = array('name' => $var['name'], 'pic' => $var['pic'], 'resp' => $var['resp']);
        $query = "INSERT INTO doc_documents (name, pic, done, resp) VALUES (:name, :pic, 0, :resp)";
        $db->query($query, function($STH) {
                    
                }, function($err) {
                    return $err;
                }, $data);
        $query = "SELECT id FROM doc_documents WHERE name = :name";
        $db->query($query, function($STH) {
                    
                }, function($err) {
                    return $err;
                }, array('name' => $var['name']));
        $id = $db->STH->fetchObject()->id;
        DocumentsV0_1::Assign(array('doc' => $id, 'user' => $var['resp']));
        //return $db;
    }
    
    /**
     * (PHP 4, PHP 5)<br/>
     * Edits selected document
     * @link http://api.asoiu.com/docs/document
     * @param int $id <p>
     * ID of the document to edit
     * @param string $name <p>
     * New name of the document
     * @param link $pic <p>
     * New link to the picture of the document
     * @param bool $done <p>
     * 1 if document was processed, 0 if wasn't
     * @param int $resp <p>
     * ID of new responsible user.
     * </p>
     * @return nothing.
     */
    public static function Edit($var) {
        $db = db_config::getInstance();
        
        $data = array('id' => $var['id'], 'name' => $var['name'], 'pic' => $var['pic'], 
                'done' => $var['done'], 'resp' => $var['resp']);
        $query = "UPDATE doc_documents SET name = :name, pic = :pic, done = :done, resp = :resp WHERE id = :id";
        $db->query($query, function($STH) {

                }, function($err) {
                    return $err;
                }, $data);
        //return $db;
    }
    
    /**
     * (PHP 4, PHP 5)<br/>
     * Deletes selected document and its assignments
     * @link http://api.asoiu.com/docs/document
     * @param int $id <p>
     * ID of the document to delete
     * </p>
     * @return nothing.
     */
    public static function Delete($var) {
        $db = db_config::getInstance();
        $query = "DELETE FROM doc_documents WHERE id = :id";
        $db->query($query, function($STH) {
                    
                }, function($err) {
                    return $err;
                }, array('id' => $var['id']));
        $query = "DELETE FROM doc_assignment WHERE doc = :id";
        $db->query($query, function($STH) {
                    
                }, function($err) {
                    return $err;
                }, array('id' => $var['id']));
        //return $db;
    }
    
    /**
     * (PHP 4, PHP 5)<br/>
     * Assigns user to the document
     * @link http://api.asoiu.com/docs/document
     * @param int $doc <p>
     * ID of the assigning document
     * @param int $user <p>
     * ID of the user to assign
     * </p>
     * @return nothing.
     */
    public static function Assign($var) {
        $db = db_config::getInstance();

        if(self::CheckDoc($var['doc'])){
            return new Error(506,"Doc not found");
        }
        $data = array('doc' => $var['doc'], 'user' => $var['user']);
        $query = "INSERT INTO doc_assignment (doc, user) VALUES (:doc, :user)";
        $db->query($query, function($STH) {

                }, function($err) {
                    return $err;
                }, $data);
        //return $db;
    }
    
    /**
     * (PHP 4, PHP 5)<br/>
     * Dessigns user from the document
     * @link http://api.asoiu.com/docs/document
     * @param int $doc <p>
     * ID of the deassigning document
     * @param int $user <p>
     * ID of the user to deassign
     * </p>
     * @return nothing.
     */
    public static function Deassign($var) {
        $db = db_config::getInstance();

        if($this->CheckDoc($var['doc'])){
            return new Error(506,"Doc not found");
        }
        $data = array('doc' => $var['doc'], 'user' => $var['user']);
        $query = "DELETE FROM doc_assignment WHERE doc = :doc AND user = :user";
        $db->query($query, function($STH) {
                    
                }, function($err) {
                    return $err;
                }, $data);
        //return $db;
    }
    
    //tmp
    
    private function CheckName($name) {
        if (!empty($name) || strlen($name) > 256)
            return false;
        return true;
    }
    
    private function CheckLink($link){
        if (!empty($link) || strlen($link) > 512)
            return false;
        return true;
    }
    
    private function CheckResp($id){
        $db = db_config::getInstance();
        $query = "SELECT FROM users WHERE resp = :id";
        $db->query($query, function($STH) {
                }, function($err) {
                    return $err;
                }, array('id' => $id));
        if($db->STH->rowCount()>0){
            return false;
        }
        return true;
    }
    
     private function CheckID($id){
        $db = db_config::getInstance();
        $query = "SELECT FROM doc_documents WHERE id = :id";
        $db->query($query, function($STH) {
                }, function($err) {
                    return $err;
                }, array('id' => $id));
        if($db->STH->rowCount()>0){
            return false;
        }
        return true;
    }
    
     private function CheckDoc($id){
        $db = db_config::getInstance();
        $query = "SELECT FROM doc_documents WHERE doc = :id";
        $db->query($query, function($STH) {
                }, function($err) {
                    return $err;
                }, array('id' => $id));
        if($db->STH->rowCount()>0){
            return false;
        }
        return true;
    }
    
}

?>
