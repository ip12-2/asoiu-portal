<?php

/**
 *
 * @author Variecs
 * @since 24.03.14 18:00
 *
 */
interface DocumentsInterface {
    const version = "0_1";
    const isAT = true; //is need access_token;

    public static function Get($arr);

    public static function Create($arr);
    
    public static function Edit($arr);
    
    public static function Delete($arr);

    public static function Assign($arr);

    public static function Deassign($arr);
}

?>
