<?php
include_once "../api/Engine/API.php";
include_once "DocumentsV0_1.php";

class APIDocumentsV0_1 implements API {

    public function APIMethod() {
        return new DocumentsV0_1();
    }

}